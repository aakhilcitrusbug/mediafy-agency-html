
if ($( window ).width() >= 1025) {
var controller = new ScrollMagic.Controller();




    new ScrollMagic.Scene({
        triggerElement: "#partner",
        triggerHook: 0.8, // show, when scrolled 10% into view
        duration: "250%", // hide 10% before exiting view (80% + 10% from bottom)
        offset: 0, // move trigger to center of element
    
    })
    // .addIndicators (
    //     {name: "reveal-bg-1" }
    // )
    .setClassToggle(".reveal-bg-1", "visible") // add class to reveal
    .addTo(controller);

    // dd

    new ScrollMagic.Scene({
        triggerElement: "#case-study",
        triggerHook: 0.8, // show, when scrolled 10% into view
        duration: "180%", // hide 10% before exiting view (80% + 10% from bottom)
        offset: 0, // move trigger to center of element
    
    })
    // .addIndicators (
    //     {name: "reveal-bg-2" }
    // )
    .setClassToggle(".reveal-bg-2", "visible") // add class to reveal
    .addTo(controller);

    // 

    new ScrollMagic.Scene({
        triggerElement: "#our-approch",
        triggerHook: 0.8, // show, when scrolled 10% into view
        duration: "230%", // hide 10% before exiting view (80% + 10% from bottom)
        offset: 0, // move trigger to center of element
    
    })
    // .addIndicators (
    //     {name: "reveal-bg-3" }
    // )
    .setClassToggle(".reveal-bg-3", "visible") // add class to reveal
    .addTo(controller);

// 
    new ScrollMagic.Scene({
        triggerElement: "#dark-case",
        triggerHook: 0.9, // show, when scrolled 10% into view
        duration: "190%", // hide 10% before exiting view (80% + 10% from bottom)
        offset: 0, // move trigger to center of element
    
    })
    // .addIndicators (
    //     {name: "reveal-bg-4" }
    // )
    .setClassToggle(".reveal-bg-4", "visible") // add class to reveal
    .addTo(controller);


    // 
    new ScrollMagic.Scene({
        triggerElement: "#capability",
        triggerHook: 0.8, // show, when scrolled 10% into view
        duration: "200%", // hide 10% before exiting view (80% + 10% from bottom)
        offset: 0, // move trigger to center of element
    
    })
    // .addIndicators (
    //     {name: "reveal-bg-5" }
    // )
    .setClassToggle(".reveal-bg-5", "visible") // add class to reveal
    .addTo(controller);


    
    // 
    new ScrollMagic.Scene({
        triggerElement: "#casestudy-white",
        triggerHook: 0.8, // show, when scrolled 10% into view
        duration: "200%", // hide 10% before exiting view (80% + 10% from bottom)
        offset: 0, // move trigger to center of element
    
    })
    // .addIndicators (
    //     {name: "reveal-bg-6" }
    // )
    .setClassToggle(".reveal-bg-6", "visible") // add class to reveal
    .addTo(controller);

        // 
        new ScrollMagic.Scene({
            triggerElement: "#casestudy-white",
            triggerHook: 0.8, // show, when scrolled 10% into view
            duration: "300%", // hide 10% before exiting view (80% + 10% from bottom)
            offset: 0, // move trigger to center of element
        
        })
        // .addIndicators (
        //     {name: "reveal-bg-6" }
        // )
        .setClassToggle(".reveal-bg-6", "visible") // add class to reveal
        .addTo(controller);
    
          // 
          new ScrollMagic.Scene({
            triggerElement: "#brand-logo",
            triggerHook: 0.8, // show, when scrolled 10% into view
            duration: "200%", // hide 10% before exiting view (80% + 10% from bottom)
            offset: 0, // move trigger to center of element
        
        })
        // .addIndicators (
        //     {name: "reveal-bg-7" }
        // )
        .setClassToggle(".reveal-bg-7", "visible") // add class to reveal
        .addTo(controller);
    

              // 
        new ScrollMagic.Scene({
            triggerElement: "#our-growth",
            triggerHook: 0.8, // show, when scrolled 10% into view
            duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
            offset: 0, // move trigger to center of element
        
        })
        // .addIndicators (
        //     {name: "reveal-bg-8" }
        // )
        .setClassToggle(".reveal-bg-8", "visible") // add class to reveal
        .addTo(controller);
    
        // 
        new ScrollMagic.Scene({
            triggerElement: "#footer",
            triggerHook: 0.8, // show, when scrolled 10% into view
            duration: "120%", // hide 10% before exiting view (80% + 10% from bottom)
            offset: 0, // move trigger to center of element
        
        })
        // .addIndicators (
        //     {name: "reveal-footer" }
        // )
        .setClassToggle(".reveal-footer", "visible") // add class to reveal
        .addTo(controller);
    


        }


        if ($( window ).width() <= 1024) {
            var controller = new ScrollMagic.Controller();
            
            
            
            
                new ScrollMagic.Scene({
                    triggerElement: "#partner",
                    triggerHook: 0.8, // show, when scrolled 10% into view
                    duration: "240%", // hide 10% before exiting view (80% + 10% from bottom)
                    offset: 0, // move trigger to center of element
                
                })
                // .addIndicators (
                //     {name: "reveal-bg-1" }
                // )
                .setClassToggle(".reveal-bg-1", "visible") // add class to reveal
                .addTo(controller);
            
                // dd
            
                new ScrollMagic.Scene({
                    triggerElement: "#case-study",
                    triggerHook: 0.8, // show, when scrolled 10% into view
                    duration: "180%", // hide 10% before exiting view (80% + 10% from bottom)
                    offset: 0, // move trigger to center of element
                
                })
                // .addIndicators (
                //     {name: "reveal-bg-2" }
                // )
                .setClassToggle(".reveal-bg-2", "visible") // add class to reveal
                .addTo(controller);
            
                // 
            
                new ScrollMagic.Scene({
                    triggerElement: "#our-approch",
                    triggerHook: 0.8, // show, when scrolled 10% into view
                    duration: "230%", // hide 10% before exiting view (80% + 10% from bottom)
                    offset: 0, // move trigger to center of element
                
                })
                // .addIndicators (
                //     {name: "reveal-bg-3" }
                // )
                .setClassToggle(".reveal-bg-3", "visible") // add class to reveal
                .addTo(controller);
            
            // 
                new ScrollMagic.Scene({
                    triggerElement: "#dark-case",
                    triggerHook: 0.8, // show, when scrolled 10% into view
                    duration: "260%", // hide 10% before exiting view (80% + 10% from bottom)
                    offset: 0, // move trigger to center of element
                
                })
                // .addIndicators (
                //     {name: "reveal-bg-4" }
                // )
                .setClassToggle(".reveal-bg-4", "visible") // add class to reveal
                .addTo(controller);
            
            
                // 
                new ScrollMagic.Scene({
                    triggerElement: "#capability",
                    triggerHook: 0.8, // show, when scrolled 10% into view
                    duration: "200%", // hide 10% before exiting view (80% + 10% from bottom)
                    offset: 0, // move trigger to center of element
                
                })
                // .addIndicators (
                //     {name: "reveal-bg-5" }
                // )
                .setClassToggle(".reveal-bg-5", "visible") // add class to reveal
                .addTo(controller);
            
            
                
                // 
                new ScrollMagic.Scene({
                    triggerElement: "#casestudy-white",
                    triggerHook: 0.8, // show, when scrolled 10% into view
                    duration: "200%", // hide 10% before exiting view (80% + 10% from bottom)
                    offset: 0, // move trigger to center of element
                
                })
                // .addIndicators (
                //     {name: "reveal-bg-6" }
                // )
                .setClassToggle(".reveal-bg-6", "visible") // add class to reveal
                .addTo(controller);
            
                    // 
                    new ScrollMagic.Scene({
                        triggerElement: "#casestudy-white",
                        triggerHook: 0.8, // show, when scrolled 10% into view
                        duration: "300%", // hide 10% before exiting view (80% + 10% from bottom)
                        offset: 0, // move trigger to center of element
                    
                    })
                    // .addIndicators (
                    //     {name: "reveal-bg-6" }
                    // )
                    .setClassToggle(".reveal-bg-6", "visible") // add class to reveal
                    .addTo(controller);
                
                      // 
                      new ScrollMagic.Scene({
                        triggerElement: "#brand-logo",
                        triggerHook: 0.8, // show, when scrolled 10% into view
                        duration: "200%", // hide 10% before exiting view (80% + 10% from bottom)
                        offset: 0, // move trigger to center of element
                    
                    })
                    // .addIndicators (
                    //     {name: "reveal-bg-7" }
                    // )
                    .setClassToggle(".reveal-bg-7", "visible") // add class to reveal
                    .addTo(controller);
                
            
                          // 
                    new ScrollMagic.Scene({
                        triggerElement: "#our-growth",
                        triggerHook: 0.8, // show, when scrolled 10% into view
                        duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
                        offset: 0, // move trigger to center of element
                    
                    })
                    // .addIndicators (
                    //     {name: "reveal-bg-8" }
                    // )
                    .setClassToggle(".reveal-bg-8", "visible") // add class to reveal
                    .addTo(controller);
                
                    // 
                    new ScrollMagic.Scene({
                        triggerElement: "#footer",
                        triggerHook: 0.8, // show, when scrolled 10% into view
                        duration: "120%", // hide 10% before exiting view (80% + 10% from bottom)
                        offset: 0, // move trigger to center of element
                    
                    })
                    // .addIndicators (
                    //     {name: "reveal-footer" }
                    // )
                    .setClassToggle(".reveal-footer", "visible") // add class to reveal
                    .addTo(controller);
                
            
            
                    }
        
if ($( window ).width() <= 768) {
    var controller = new ScrollMagic.Controller();
    
    
    
    
        new ScrollMagic.Scene({
            triggerElement: "#partner",
            triggerHook: 0.8, // show, when scrolled 10% into view
            duration: "230%", // hide 10% before exiting view (80% + 10% from bottom)
            offset: 0, // move trigger to center of element
        
        })
        // .addIndicators (
        //     {name: "reveal-bg-1" }
        // )
        .setClassToggle(".reveal-bg-1", "visible") // add class to reveal
        .addTo(controller);
    
        // dd
    
        new ScrollMagic.Scene({
            triggerElement: "#case-study",
            triggerHook: 0.8, // show, when scrolled 10% into view
            duration: "180%", // hide 10% before exiting view (80% + 10% from bottom)
            offset: 0, // move trigger to center of element
        
        })
        // .addIndicators (
        //     {name: "reveal-bg-2" }
        // )
        .setClassToggle(".reveal-bg-2", "visible") // add class to reveal
        .addTo(controller);
    
        // 
    
        new ScrollMagic.Scene({
            triggerElement: "#our-approch",
            triggerHook: 0.8, // show, when scrolled 10% into view
            duration: "230%", // hide 10% before exiting view (80% + 10% from bottom)
            offset: 0, // move trigger to center of element
        
        })
        // .addIndicators (
        //     {name: "reveal-bg-3" }
        // )
        .setClassToggle(".reveal-bg-3", "visible") // add class to reveal
        .addTo(controller);
    
    // 
        new ScrollMagic.Scene({
            triggerElement: "#dark-case",
            triggerHook: 0.8, // show, when scrolled 10% into view
            duration: "260%", // hide 10% before exiting view (80% + 10% from bottom)
            offset: 0, // move trigger to center of element
        
        })
        // .addIndicators (
        //     {name: "reveal-bg-4" }
        // )
        .setClassToggle(".reveal-bg-4", "visible") // add class to reveal
        .addTo(controller);
    
    
        // 
        new ScrollMagic.Scene({
            triggerElement: "#capability",
            triggerHook: 0.8, // show, when scrolled 10% into view
            duration: "200%", // hide 10% before exiting view (80% + 10% from bottom)
            offset: 0, // move trigger to center of element
        
        })
        // .addIndicators (
        //     {name: "reveal-bg-5" }
        // )
        .setClassToggle(".reveal-bg-5", "visible") // add class to reveal
        .addTo(controller);
    
    
        
        // 
        new ScrollMagic.Scene({
            triggerElement: "#casestudy-white",
            triggerHook: 0.8, // show, when scrolled 10% into view
            duration: "200%", // hide 10% before exiting view (80% + 10% from bottom)
            offset: 0, // move trigger to center of element
        
        })
        // .addIndicators (
        //     {name: "reveal-bg-6" }
        // )
        .setClassToggle(".reveal-bg-6", "visible") // add class to reveal
        .addTo(controller);
    
            // 
            new ScrollMagic.Scene({
                triggerElement: "#casestudy-white",
                triggerHook: 0.8, // show, when scrolled 10% into view
                duration: "300%", // hide 10% before exiting view (80% + 10% from bottom)
                offset: 0, // move trigger to center of element
            
            })
            // .addIndicators (
            //     {name: "reveal-bg-6" }
            // )
            .setClassToggle(".reveal-bg-6", "visible") // add class to reveal
            .addTo(controller);
        
              // 
              new ScrollMagic.Scene({
                triggerElement: "#brand-logo",
                triggerHook: 0.8, // show, when scrolled 10% into view
                duration: "200%", // hide 10% before exiting view (80% + 10% from bottom)
                offset: 0, // move trigger to center of element
            
            })
            // .addIndicators (
            //     {name: "reveal-bg-7" }
            // )
            .setClassToggle(".reveal-bg-7", "visible") // add class to reveal
            .addTo(controller);
        
    
                  // 
            new ScrollMagic.Scene({
                triggerElement: "#our-growth",
                triggerHook: 0.8, // show, when scrolled 10% into view
                duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
                offset: 0, // move trigger to center of element
            
            })
            // .addIndicators (
            //     {name: "reveal-bg-8" }
            // )
            .setClassToggle(".reveal-bg-8", "visible") // add class to reveal
            .addTo(controller);
        
            // 
            new ScrollMagic.Scene({
                triggerElement: "#footer",
                triggerHook: 0.8, // show, when scrolled 10% into view
                duration: "120%", // hide 10% before exiting view (80% + 10% from bottom)
                offset: 0, // move trigger to center of element
            
            })
            // .addIndicators (
            //     {name: "reveal-footer" }
            // )
            .setClassToggle(".reveal-footer", "visible") // add class to reveal
            .addTo(controller);
        
    
    
            }